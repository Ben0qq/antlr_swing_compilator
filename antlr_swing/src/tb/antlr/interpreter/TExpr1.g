tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (print | set | add);

print   :
          ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());} | 
          e=expr {drukuj ($e.text);}
          ;
add     :
          ^(VAR i1=ID) {addVar($ID.text);}
          ;
set     :
          ^(PODST i1=ID e2=expr) {setVar($i1.text, $e2.out);}
          ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(POW   e1=expr e2=expr) {$out = potega($e1.out,$e2.out);}
        | ^(AND   e1=expr e2=expr) {$out = $e1.out & $e2.out;}
        | ^(OR    e1=expr e2=expr) {$out = $e1.out | $e2.out;}
        | ID                       {$out = getVar($ID.text);}
        | INT                      {$out = getInt($INT.text);}
        ;
