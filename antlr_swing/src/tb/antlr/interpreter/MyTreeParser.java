package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import java.lang.Math;
import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {

	protected GlobalSymbols globals = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }
    
    protected Integer potega(int x, int y) {
    	return (int)Math.pow(x, y);
    }
    
    public void addVar(String name) {
    	if(!globals.hasSymbol(name)) globals.newSymbol(name);
    }
    
    public void setVar(String name, Integer value) {
    	if(globals.hasSymbol(name)) globals.setSymbol(name, value);
    }
    
    public Integer getVar(String name) {
    	return globals.getSymbol(name);
    }
    
	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}
}
